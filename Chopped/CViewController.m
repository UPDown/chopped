//
//  CViewController.m
//  Chopped
//
//

//  Created by helfy on 14-5-23.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CViewController.h"

#import "CChoopedView.h"

#import "CCoverLayer.h"

#import "UIImage+Resize.h"
#import "MBProgressHUD.h"

#import "CChoppedObj.h"
#import "CChoppedDataBase.h"
#import "CImageMangerViewController.h"

#import "CMeunView.h"
#import "CMenuItem.h"
//#import "ALAssetsLibrary+CustomPhotoAlbum.h"
//#import <MobileCoreServices/MobileCoreServices.h>

#import "CSaveImages.h"
#import "UMFeedbackViewController.h"

#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"
#import "CAppDelegate.h"
#import "MIADViewer.h"
#import "CLoadAnimation.h"

#import "MobClick.h"

static  float CropMaxWidth = 0.0;
static  float CropMaxHeight = 0.0;
static  CGPoint CropCenter;
@interface CViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate,UIScrollViewDelegate,MBProgressHUDDelegate>
{
    UINavigationBar *navBarView;
    UIImageView *editImageView;
    
    UIView *toolButtomView;
    UIScrollView  *scrollview;
    CGSize      cropSize;
    BOOL        isDashed;
    
    CCoverLayer *coverLayer;

    
    MBProgressHUD *HUD;
    
    CChoppedObj *currentEditObj;
    
    CMeunView *menu;
    CLoadAnimation*iconBgView;
    
//    ALAssetsLibrary     * assetsLibrary_;
}
//@property (nonatomic, strong) ALAssetsLibrary     * assetsLibrary;
@end

@implementation CViewController
//@synthesize assetsLibrary        = assetsLibrary_;

-(NSString *)get_uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(nil);
    CFStringRef stringRef = CFUUIDCreateString(nil, uuidRef);
    NSString *uuid = [NSString stringWithString:(__bridge NSString*)stringRef];
    CFRelease(uuidRef);
    CFRelease(stringRef);
    
    return uuid;
}


-(NSTimeInterval)get_Time
{
    NSDate *date = [NSDate date];
    return [date timeIntervalSince1970];
}



/*
- (ALAssetsLibrary *)assetsLibrary
{
    if (assetsLibrary_) {
        return assetsLibrary_;
    }
    assetsLibrary_ = [[ALAssetsLibrary alloc] init];
    return assetsLibrary_;
}
*/

#pragma mark - -Action
- (void)managerAction
{
    CAppDelegate *delegate = (CAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.drawerController setCenterViewController:delegate.drawerController.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        CImageMangerViewController *VC = [[CImageMangerViewController alloc] initWithNibName:nil bundle:nil];
        [self presentViewController:VC animated:YES completion:nil];
    }];
    
}

- (void)feedBackAction
{

    CAppDelegate *delegate = (CAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.drawerController setCenterViewController:delegate.drawerController.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        
        UMFeedbackViewController *feedbackViewController = [[UMFeedbackViewController alloc] initWithNibName:@"UMFeedbackViewController" bundle:nil];
        feedbackViewController.appkey = UMENG_APPKEY;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:feedbackViewController];
                
        [self presentViewController:navigationController animated:YES completion:nil];
    }];
}

- (void)seamlessModeAction:(UIButton *)sender
{
//    CAppDelegate *delegate = (CAppDelegate *)[[UIApplication sharedApplication] delegate];
//    [delegate.drawerController setCenterViewController:delegate.drawerController.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
        if (coverLayer) {
            if (coverLayer) {
                isDashed = !isDashed;
                [[NSUserDefaults standardUserDefaults] setBool:isDashed forKey:kIsDashedKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [coverLayer setIsDashed:isDashed];
            }
        }
//    }];
}

- (void)OKAction
{
    if (coverLayer) {
        

        [MobClick event:@"FinishCrop"];
        
        if (HUD) {
            HUD.delegate = nil;
            [HUD removeFromSuperview];
            HUD = nil;
        }
        HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
        [self.view.window addSubview:HUD];
        HUD.dimBackground = YES;
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        HUD.delegate = self;
//        HUD.labelText = @"处理中……";
        CLoadAnimation *animatioView = [[CLoadAnimation alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        HUD.customView=animatioView;
        HUD.mode = MBProgressHUDModeCustomView;
        HUD.contentMode = UIViewContentModeCenter;
        [HUD show:YES];
        CGRect rect = [coverLayer convertRect:coverLayer.bounds toView:editImageView];
        //截图
       
        NSString *subPath = [currentEditObj getObjSavePath];
        if (![[NSFileManager defaultManager] fileExistsAtPath:subPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:subPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        //TODO 缩略图
        UIImage *thumbImage = [self getThumbnails:scrollview.frame];
        NSData *data = UIImagePNGRepresentation(thumbImage);
        
        currentEditObj.thumbImageHeigth = thumbImage.size.height/2;
        currentEditObj.thumbImageWidht = thumbImage.size.width/2;
       
        //TODO  判断。。用户设定
        
        BOOL saveMode = [[NSUserDefaults standardUserDefaults] boolForKey:kSaveModeKey];
        if(saveMode)
        {
            [currentEditObj choppedImageFinish];
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [data writeToFile:[currentEditObj getThumbImagePath] atomically:YES];
            NSArray *cropImageArray = [self getCroppedImage:rect chopped:cropSize];
            [CSaveImages saveImages:cropImageArray toPath:subPath];
           dispatch_sync(dispatch_get_main_queue(), ^{
               [HUD hide:YES afterDelay:1];
                [currentEditObj reSetId];
           });
        });
        }
        else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
             NSArray *cropImageArray = [self getCroppedImage:rect chopped:cropSize];
                 dispatch_sync(dispatch_get_main_queue(), ^{
            [CSaveImages saveImagesForImageArray:cropImageArray forIndex:0 completion:^{
                 [HUD hide:YES afterDelay:1];
                [currentEditObj reSetId];
            }];
              
                 });
            });
            
        }
        
     
    }
    
}

- (void)changeLineRow
{
    //    CAppDelegate *delegate = (CAppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [delegate.drawerController setCenterViewController:delegate.drawerController.centerViewController withFullCloseAnimation:YES completion:^(BOOL finished) {
    if(currentEditObj.choppedImage ==nil)
    {
        [self chooseImage];
        return;
    }
    
    UIView *pickerBgView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:pickerBgView];
    pickerBgView.backgroundColor =  [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.3];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenPicker:)];
    [pickerBgView addGestureRecognizer:tap];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,pickerBgView.frame.size.height-216, pickerBgView.frame.size.width, 216)];
    pickerView.backgroundColor =[UIColor whiteColor];
    pickerView.dataSource = (id)self;   //
    pickerView.delegate = (id)self;       //
    pickerView.showsSelectionIndicator = YES;
    
    [pickerBgView addSubview:pickerView];
    pickerView.tag = 123;
    
    pickerView.frame = CGRectMake(0,pickerBgView.frame.size.height, pickerBgView.frame.size.width, 216);
    pickerBgView.backgroundColor =  [UIColor clearColor];
    [UIView beginAnimations:nil context:nil];
    pickerView.frame = CGRectMake(0,pickerBgView.frame.size.height-216, pickerBgView.frame.size.width, 216);
    pickerBgView.backgroundColor =  [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.3];
    [UIView commitAnimations];
    //    }];
    
}


-(void)chooseImage
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"选择图片获取方式"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"相册", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"相机", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"取消", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    [actionSheet showInView:self.view];
}

#pragma mark - -图片处理
//合并图片
-(UIImage *)mergerImage:(UIImage *)firstImage secodImage:(UIImage *)secondImage{
    
    CGSize imageSize = CGSizeMake(620, 380);
    UIGraphicsBeginImageContext(imageSize);
    
    [firstImage drawInRect:CGRectMake(0, 0, firstImage.size.width, firstImage.size.height)];
    [secondImage drawInRect:CGRectMake(310 - 40, 190 - 60, secondImage.size.width, secondImage.size.height)];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

//截图
- (NSMutableArray*) getCroppedImage:(CGRect)rect chopped:(CGSize)choppedSize
{
    NSMutableArray *arrary = [[NSMutableArray alloc] init];
    if (isDashed) {
        //无缝
        float itemWidth = rect.size.width/currentEditObj.columnCount;
        float itemHeight = rect.size.height/currentEditObj.rowCount;
        
        for (int i = 0; i < currentEditObj.rowCount; i++) {
            
            for (int j = 0; j< currentEditObj.columnCount; j++) {

                CGRect rectd = CGRectMake(rect.origin.x + j * itemWidth, rect.origin.y + i * itemHeight, itemWidth, itemHeight);
                // grab image
                CGImageRef imgrefout = CGImageCreateWithImageInRect([editImageView.image CGImage], rectd);
                UIImage *croppedImage = [[UIImage alloc]initWithCGImage:imgrefout];
                UIImage *newImage = [croppedImage resizedImage:cropSize interpolationQuality:0];
                [arrary addObject:newImage];
                CGImageRelease(imgrefout);
            }
            
        }
    }
    else{
        //
        CGRect lineRect = [coverLayer convertRect:CGRectMake(0, 0, LineWidth, LineWidth) toView:editImageView];
        float itemWidth = (rect.size.width - (currentEditObj.columnCount - 1) * lineRect.size.width)/currentEditObj.columnCount;
        float itemHeight = (rect.size.height - (currentEditObj.rowCount - 1) * lineRect.size.height)/currentEditObj.rowCount;
        
        for (int i = 0; i < currentEditObj.rowCount; i++) {
            
            for (int j = 0; j< currentEditObj.columnCount; j++) {

                CGRect rectd = CGRectMake(rect.origin.x + (itemWidth + lineRect.size.width) * j, rect.origin.y + (itemHeight + lineRect.size.height) * i, itemWidth, itemHeight);
                // grab image
                
                CGImageRef imgrefout = CGImageCreateWithImageInRect([editImageView.image CGImage], rectd);
                UIImage *croppedImage = [[UIImage alloc]initWithCGImage:imgrefout];
                UIImage *newImage = [croppedImage resizedImage:cropSize interpolationQuality:0];
                [arrary addObject:newImage];
                CGImageRelease(imgrefout);
            }
            
        }
        
    }
    

    return arrary;
}

// 缩略图
- (UIImage*) getThumbnails:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 2.0);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *uiImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImage *img = [uiImage croppedImage:CGRectMake(rect.origin.x * 2.0, rect.origin.y * 2.0, rect.size.width * 2, rect.size.height * 2)];
    UIGraphicsEndImageContext();
    
    return img;
}


#pragma  mark - -View
-(void)setupLeftMenuButton{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

-(void)setupRightMenuButton{
    UIBarButtonItem *rightBarButtonItem  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(chooseImage)];
    [self.navigationItem setRightBarButtonItem:rightBarButtonItem animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    if(kDevIsIOS7)
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
        UIColor * barColor = [UIColor
                              colorWithRed:247.0/255.0
                              green:249.0/255.0
                              blue:250.0/255.0
                              alpha:1.0];
        [self.navigationController.navigationBar setBarTintColor:barColor];
    }
    else {
        UIColor * barColor = [UIColor
                              colorWithRed:78.0/255.0
                              green:156.0/255.0
                              blue:206.0/255.0
                              alpha:1.0];
        [self.navigationController.navigationBar setTintColor:barColor];
    }
    
    [self setupLeftMenuButton];
    [self setupRightMenuButton];
    self.view.backgroundColor = RGB(242, 242, 242, 1);
    currentEditObj = [[CChoppedObj alloc] init];
    currentEditObj.delegate = (id)self;
    cropSize = CGSizeMake(180, 180);
    isDashed = [[NSUserDefaults standardUserDefaults] boolForKey:kIsDashedKey];
    float offsetY = 10+(kDevIsIOS7?(45+20):0) + (kDecIsiPhone5?20:0);
    float offsetX = 40-(kDecIsiPhone5?20:0);
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(offsetX,offsetY+10, CGRectGetWidth(self.view.bounds)- offsetX*2, CGRectGetWidth(self.view.bounds)- offsetX*2)];
    scrollview.backgroundColor = [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1];
    scrollview.delegate = self;
    [self.view addSubview:scrollview];
    [scrollview setZoomScale:1.0];
    scrollview.contentSize =scrollview.frame.size;
    scrollview.showsHorizontalScrollIndicator = false;
    scrollview.showsVerticalScrollIndicator = false;
    iconBgView = [[CLoadAnimation alloc] initWithFrameForAutoWidth:scrollview.frame];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseImage)];
    [iconBgView addGestureRecognizer:tap];
    
    [self.view addSubview:iconBgView];
    CropMaxHeight = CGRectGetHeight(scrollview.frame);
    CropMaxWidth = CGRectGetWidth(scrollview.frame);
    CropCenter = scrollview.center;

    menu = [[CMeunView alloc] initWithFrame:CGRectMake(0,  CGRectGetMinY(scrollview.frame) + CGRectGetHeight(scrollview.frame) + + (kDecIsiPhone5?30:20), 320, 74)];
    menu.menuDelegate = (id)self;
    [self.view addSubview:menu];
    menu.hidden = YES;
    
    MIADViewer *Adview = [MIADViewer shareADView];
    Adview.delegate =(id) self;
    [self.view addSubview:Adview];
    
    

}
-(void)adLoadFilish:(id)view
{
    MIADViewer *Adview = [MIADViewer shareADView];
    Adview.frame =CGRectMake(0, CGRectGetHeight(self.view.frame)-Adview.frame.size.height, CGRectGetWidth(self.view.frame), Adview.frame.size.height);
}
-(void)loadAdError:(NSString *)error
{

}


-(void)menuItemDidTap:(CMenuItem *)item;
{
    int itemIndex =item.tag;
    switch (itemIndex) {
        case 0:
            [self changeLineRow];
            break;
        case 1:
         
            [self seamlessModeAction:nil];
            [item setTitle:isDashed?@"无缝":@"有缝" subTitle:nil];
            break;
        case 2:
           [self OKAction];
            break;
        default:
            break;
    }
}

-(void)hiddenPicker:(UITapGestureRecognizer *)tap
{
    UIView *pickerBgView =tap.view;
    UIPickerView *pickerView =(UIPickerView *)[pickerBgView viewWithTag:123];
    [UIView animateWithDuration:0.25 animations:^{
     
        
        pickerView.frame = CGRectMake(0,pickerBgView.frame.size.height, pickerBgView.frame.size.width, 216);
        pickerBgView.backgroundColor =  [UIColor clearColor];
    } completion:^(BOOL finished) {
          [pickerBgView removeFromSuperview];
    }];
  
}

- (void)showCamera
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:controller animated:YES completion:NULL];
  
}

- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:controller animated:YES completion:NULL];
}
#pragma Mark UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"相册", nil)]) {
        [self openPhotoAlbum];
    } else if ([buttonTitle isEqualToString:NSLocalizedString(@"相机", nil)]) {
        [self showCamera];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    [currentEditObj setNewImage:image];
    //TODO menu 动画
    menu.hidden = NO;
    iconBgView.hidden = YES;
   
    [picker dismissViewControllerAnimated:YES completion:^{
        if (!coverLayer)
        {
            coverLayer = [[CCoverLayer alloc] initWithFrame:scrollview.frame];
            coverLayer.backgroundColor = [UIColor clearColor];
            coverLayer.userInteractionEnabled = NO;
            [self.view addSubview:coverLayer];
             [coverLayer setIsDashed:isDashed];
        }
        [coverLayer setLine:currentEditObj.columnCount withRow:currentEditObj.rowCount];
    }];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark -
#pragma mark UIScrollViewDelegate methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return editImageView;
}


#pragma mark -
#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	HUD = nil;
}
#pragma mark -

#pragma mark UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView

{
    return 2;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}
#pragma mark UIPickerViewDelegate

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{


    if(component ==0 && row == currentEditObj.rowCount-1)
    {
        [pickerView selectRow:currentEditObj.rowCount-1 inComponent:component animated:NO];
   
    }
    else if(component ==1 && row == currentEditObj.columnCount-1){
        [pickerView selectRow:currentEditObj.columnCount-1 inComponent:component animated:NO];
        
    }
      if(component ==0)
      {
          return [NSString stringWithFormat: @"%i 行",row+1];
      }
      else
      {
          return [NSString stringWithFormat: @"%i 列",row+1];
      }
    return [NSString stringWithFormat: @"%i 列",row+1];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(component ==0)
    {
        [currentEditObj setNewRow: row+1 column: currentEditObj.columnCount];
    }
    else{
         [currentEditObj setNewRow: currentEditObj.rowCount column:row+1];
        
    }
    
}

#pragma mark CChoppedObj Delegate methods
-(void)rowAndColumnDidChange
{
    
    [UIView beginAnimations:nil context:nil];
    if (currentEditObj.columnCount == currentEditObj.rowCount) {
        
        scrollview.frame = CGRectMake((CGRectGetWidth(self.view.frame) - CropMaxWidth)/2.0, scrollview.frame.origin.y, CropMaxWidth, CropMaxHeight);
        coverLayer.frame = scrollview.frame;
    }
    else if (currentEditObj.columnCount < currentEditObj.rowCount){
        //高>宽
        
        float width = CropMaxHeight/currentEditObj.rowCount * currentEditObj.columnCount;
        scrollview.frame = CGRectMake((CGRectGetWidth(self.view.frame) - width)/2.0, scrollview.frame.origin.y, width, CropMaxHeight);
   
        coverLayer.frame = scrollview.frame;
    }
    else{
        //宽>高
        float height = CropMaxWidth/currentEditObj.columnCount * currentEditObj.rowCount;
        
        scrollview.frame = CGRectMake((CGRectGetWidth(self.view.frame) - CropMaxWidth)/2.0, scrollview.frame.origin.y, CropMaxWidth, height);
      
        coverLayer.frame = scrollview.frame;
    }
     [UIView commitAnimations];
   
    scrollview.center = CropCenter;
    coverLayer.center = CropCenter;
    if ((currentEditObj.columnCount == 1) && (currentEditObj.rowCount) == 1) {
        cropSize = CGSizeMake(300, 300);
    }
    else{
        cropSize = CGSizeMake(180, 180);
    }
    
    
    //对图片进行缩放处理
    CGSize imageSiez = currentEditObj.choppedImage.size;
    
    float wscale =CGRectGetWidth(scrollview.frame)/imageSiez.width;
    float hscale =CGRectGetHeight(scrollview.frame)/imageSiez.height;
    float minScale =wscale>hscale?wscale:hscale;
    float maxScale =wscale<hscale?wscale:hscale;
    
    scrollview.contentSize = CGSizeMake(imageSiez.width, imageSiez.height);
    if(maxScale > 1)
    {
        scrollview.maximumZoomScale =maxScale*2;
    }
    else{
        scrollview.maximumZoomScale = (1/maxScale)<2?2.0:(1/maxScale);
    }
    scrollview.minimumZoomScale = minScale;
    [scrollview setZoomScale:minScale];
    
    [coverLayer setLine:currentEditObj.columnCount withRow:currentEditObj.rowCount];

}
-(void)sizeDidChange
{

}
-(void)choppedImageChange
{
    if (editImageView) {
        [editImageView removeFromSuperview];
        editImageView = nil;
    }
    
    editImageView = [[UIImageView alloc] initWithImage:currentEditObj.choppedImage];
    editImageView.backgroundColor = [UIColor clearColor];
    [scrollview addSubview:editImageView];
    
    
    //对图片进行缩放处理
    CGSize imageSiez = currentEditObj.choppedImage.size;
    
    float wscale =CGRectGetWidth(scrollview.frame)/imageSiez.width;
    float hscale =CGRectGetHeight(scrollview.frame)/imageSiez.height;
    
    float minScale =wscale>hscale?wscale:hscale;
    float maxScale =wscale<hscale?wscale:hscale;
  
   
    
    scrollview.contentSize = CGSizeMake(imageSiez.width, imageSiez.height);
    if(maxScale > 1)
    {
        scrollview.maximumZoomScale =maxScale*2;
    }
    else{
    scrollview.maximumZoomScale = (1/maxScale)<2?2.0:(1/maxScale);
    }
    scrollview.minimumZoomScale = minScale;
    [scrollview setZoomScale:minScale];

}

@end
