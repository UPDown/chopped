//
//  CSettingViewController.h
//  Chopped
//
//  Created by helfy  on 14-5-28.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

//初始设定
//默认格子数
//格子大小 根据图片生成  自定义
//切割好的资源由app管理 或者 直接保持至相册 （原因：切割好的小图，单张图影响相册美观）
@interface CSettingViewController : UIViewController

@end
