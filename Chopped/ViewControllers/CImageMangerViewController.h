//
//  CImageMangerViewController.h
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
    切好的图片管理
 
        暂时先用tableView进行展示 每个集合包括信息：n张图片 格子个数（n＊n） 创建时间 使用次数 能预览
        批量删除
 */
@interface CImageMangerViewController : UIViewController

@end
