//
//  CImageMangerViewController.m
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CImageMangerViewController.h"
#import "CChoppedDataBase.h"
#import "CImageMangerTableViewCell.h"
#import "CChoppedObj.h"
#import "MBProgressHUD.h"
#import "CLoadAnimation.h"
#import "MobClick.h"
#import "CSaveImages.h"
@interface CImageMangerViewController ()
{
    UITableView *objTableView;
    NSMutableArray *objsArray;
    UINavigationBar* navBarView;
}
@end

@implementation CImageMangerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [MobClick event:@"EnterImageManager"];
    
    objsArray=  [[NSMutableArray alloc]initWithArray:[[CChoppedDataBase shareDBControl] queryAllChoppedObjs]];

    if(kDevIsIOS7)
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.view.backgroundColor = RGB(240,240, 240, 1);
    
    navBarView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,kDevIsIOS7?64:44 )];
    [self.view addSubview:navBarView];
    
    
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"切图管理"];
    
    item.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backToHome)];
    
    item.rightBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(editAction)];
    [navBarView pushNavigationItem:item animated:NO];
    
    objTableView =[[UITableView alloc] initWithFrame:CGRectMake(0, navBarView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-navBarView.frame.size.height)];
    [self.view addSubview:objTableView];
    objTableView.backgroundColor = [UIColor clearColor];
    objTableView.delegate = (id)self;
    objTableView.dataSource = (id)self;
    objTableView.allowsSelection = NO;
    objTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}
-(void)backToHome
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)editAction
{
//    [objTableView setEditing:YES animated:YES];
    objTableView.editing = !objTableView.editing;
    UINavigationItem *item= [[navBarView items] objectAtIndex:0];
    if(objTableView.editing )
    {
        item.rightBarButtonItem.title = @"完成";
    }
    else
    {
       item.rightBarButtonItem.title = @"编辑";
    }
}

#pragma mark - -Action
-(void)saveImageToAlbum:(CChoppedObj *)obj
{
    //保存照片至相册
    [MobClick event:@"Manager_saveImageToAlbum"];
    
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
    [self.view.window addSubview:HUD];
    HUD.dimBackground = YES;
    
    CLoadAnimation *animatioView = [[CLoadAnimation alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    HUD.customView=animatioView;
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.contentMode = UIViewContentModeCenter;
    [HUD show:YES];
    [CSaveImages saveImage:obj forIndex:0 completion:^{

        HUD.mode = MBProgressHUDModeText;
        HUD.labelText = @"已完成";
        [HUD hide:YES afterDelay:1];
        
    }];
}

-(void)deleteImageToAlbum:(CChoppedObj *)obj
{
  
//    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
//    [self.view.window addSubview:HUD];
//    HUD.dimBackground = YES;
//    HUD.contentMode = UIViewContentModeCenter;
//    [HUD show:YES];
    
    int  index = [objsArray indexOfObject:obj];
    [obj deleteSelf];
    [objsArray removeObject:obj];
    [objTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    
//    HUD.mode = MBProgressHUDModeText;
//    HUD.labelText = @"已完成";
//    [HUD hide:YES afterDelay:1.0];
    
}


#pragma mark - -UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return objsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CChoppedObj *cObj = [objsArray objectAtIndex:indexPath.row];
  
    return cObj.thumbImageHeigth + 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellStr=@"mangerCellStr";
    
    CImageMangerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellStr];
    if(cell == nil)
    {
        cell = [[CImageMangerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellStr];
        cell.saveDelegate = (id)self;
    }
    
    [cell setShowObj:[objsArray objectAtIndex:indexPath.row]];

    return cell;
}

@end
