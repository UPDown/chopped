//
//  CSettingViewController.m
//  Chopped
//
//  Created by helfy  on 14-5-28.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CSettingViewController.h"

@interface CSettingViewController ()
{
    UITableView *settingTableView;
}
@end

@implementation CSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UINavigationBar* navBarView = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,kDevIsIOS7?64:44 )];
    [self.view addSubview:navBarView];
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"设置"];
    
    item.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(backToHome)];
    [navBarView pushNavigationItem:item animated:NO];
    settingTableView =[[UITableView alloc] initWithFrame:CGRectMake(0, navBarView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-navBarView.frame.size.height)];
    [self.view addSubview:settingTableView];
    settingTableView.delegate = (id)self;
    settingTableView.dataSource = (id)self;
}
-(void)backToHome
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellStr=@"settingStr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellStr];
    if(cell == nil);
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellStr];
    }
    
    return cell;
}


@end
