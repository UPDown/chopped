//
//  CMenuViewController.m
//  Chopped
//
//  Created by HDJ on 14-6-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CMenuViewController.h"
#import "ERMeunCellObj.h"
#import "CAppDelegate.h"
#import "MBProgressHUD.h"
#import "CAppListApi.h"
#import "UIImageView+WebCache.h"
#import "CAppListCell.h"
#import "MobClick.h"

@interface CMenuViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *listTableView;
    NSMutableArray *menuCellObjArray;
    
    NSMutableArray  *appListArray;
}
@end

@implementation CMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        menuCellObjArray = [[NSMutableArray alloc] init];
        appListArray = [[NSMutableArray alloc] init];
        
        [[CAppListApi defaultApi] requestAppList:self];
        
    }
    return self;
}

//json
-(void)ERApiDidFinish:(id)api  respone:(id)jsonStr
{
    [appListArray removeAllObjects];
    
    if ([[jsonStr class] isSubclassOfClass:[NSArray class]]) {
        [appListArray addObjectsFromArray:jsonStr];
    }
    else if ([[jsonStr class] isSubclassOfClass:[NSDictionary class]]) {
        [appListArray addObject:jsonStr];
    }

    if (appListArray && appListArray.count) {
        
        if (menuCellObjArray.count>=3) {
            [menuCellObjArray removeLastObject];
        }
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dic in appListArray) {
            [array addObject:dic];
        }
        [menuCellObjArray addObject:array];
    }
    
    if(listTableView)
    {
        [listTableView reloadData];
    }
}

//获取信息失败
-(void)ERApiDidFail:(id)api  error:(NSString *)errorStr
{
    
}

- (void)dealloc
{
    menuCellObjArray = nil;
    listTableView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = RGB(242, 242, 242, 1);
    CGRect frame = self.view.bounds;
    
//    self.navigationItem.title = @"设置";
    if(kDevIsIOS7)
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
        UIColor * barColor = [UIColor
                              colorWithRed:247.0/255.0
                              green:249.0/255.0
                              blue:250.0/255.0
                              alpha:1.0];
        [self.navigationController.navigationBar setBarTintColor:barColor];
    }
    else {
        UIColor * barColor = [UIColor
                              colorWithRed:78.0/255.0
                              green:156.0/255.0
                              blue:206.0/255.0
                              alpha:1.0];
        [self.navigationController.navigationBar setTintColor:barColor];
    }
    
    [self reSetMuenCellObj];
    float offsetY = (kDevIsIOS7?(44+20):0) ;
    frame.origin.y =offsetY;
    frame.size.height = frame.size.height-offsetY;
    listTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
    listTableView.backgroundColor = [UIColor clearColor];
    listTableView.backgroundView = nil;
    listTableView.dataSource = (id)self;
    listTableView.delegate = (id)self;
//    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:listTableView];
    [listTableView setScrollsToTop:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)changeSaveMode
{
    BOOL saveMode = [[NSUserDefaults standardUserDefaults] boolForKey:kSaveModeKey];
    saveMode = !saveMode;
    [[NSUserDefaults standardUserDefaults] setBool:saveMode forKey:kSaveModeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
   MBProgressHUD* HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
    [self.view.window addSubview:HUD];
    HUD.dimBackground = YES;
    HUD.mode = MBProgressHUDModeText;

    if(saveMode)  //TODO 提示作用
    {
        HUD.labelText = @"切图保存至切图管理";
        [MobClick event:@"SavePath" label:@"应用"];
    }
    else{
        HUD.labelText = @"切图保存至相册";
        [MobClick event:@"SavePath" label:@"相册"];
    }
    [HUD show:YES];
    [HUD hide:YES afterDelay:1];
}
-(void)reSetMuenCellObj
{
    [menuCellObjArray removeAllObjects];

    NSMutableArray *array = [NSMutableArray array];
    [array addObject:[ERMeunCellObj cellObjWith:@"切图管理" level:0 action:@selector(imageManagerAction)]];
    [array addObject:[ERMeunCellObj cellObjWith:@"保存切换" level:1 action:nil]];
    [menuCellObjArray addObject:array];
    
    array = [NSMutableArray array];
    [array addObject:[ERMeunCellObj cellObjWith:@"用户反馈" level:0 action:@selector(feedBackAction)]];
    [array addObject:[ERMeunCellObj cellObjWith:@"求好评～" level:0 action:@selector(evaluateAction)]];
    [menuCellObjArray addObject:array];
    
//    array = [NSMutableArray array];
//    [array addObject:[ERMeunCellObj cellObjWith:@"小说宅" level:1 action:nil]];
//    [array addObject:[ERMeunCellObj cellObjWith:@"切切" level:1 action:@selector(qieqieAction:)]];
//    [menuCellObjArray addObject:array];

    if(listTableView.visibleCells.count>0)
    {
        [listTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    else{
        [listTableView reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = [[menuCellObjArray objectAtIndex:indexPath.section]  objectAtIndex:indexPath.row];
    if ([[obj class] isSubclassOfClass:[ERMeunCellObj class]]) {
          return 40;
    }
        else if ([[obj class] isSubclassOfClass:[NSDictionary class]]){
            return 50;
        }
    return 40;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[menuCellObjArray objectAtIndex:section] count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
       return menuCellObjArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  

    id obj = [[menuCellObjArray objectAtIndex:indexPath.section]  objectAtIndex:indexPath.row];
    UITableViewCell *cell = nil;
    if ([[obj class] isSubclassOfClass:[ERMeunCellObj class]]) {
        static NSString *CellIdentifier = @"Cell";
        
       cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        // Configure the cell...
        if(kDevIsIOS7)
        {
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }
        else
        {
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
       
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        ERMeunCellObj *cellObj = (ERMeunCellObj *)obj;
        cell.textLabel.text = cellObj.title;
        UISwitch *switchView = ( UISwitch *)[cell.contentView viewWithTag:1000];
        
        if([cellObj.title isEqualToString:@"保存切换"])
        {
            if(switchView == nil)
            {
                switchView = [[UISwitch alloc] initWithFrame:CGRectMake(180+(kDevIsIOS7?20:0), 5, 40, 30)];
                switchView.tag = 1000;
                [cell.contentView addSubview:switchView];
                [switchView addTarget:self action:@selector(changeSaveMode) forControlEvents:UIControlEventValueChanged];
                BOOL saveMode = [[NSUserDefaults standardUserDefaults] boolForKey:kSaveModeKey];
                switchView.on =saveMode;
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            switchView.hidden = NO;
        }
        else{
            switchView.hidden = YES;
        }
    }
    else if ([[obj class] isSubclassOfClass:[NSDictionary class]]){
        static NSString *CellIdentifier = @"CAppListCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CAppListCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            if(kDevIsIOS7)
            {
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            }
            else
            {
              cell.selectionStyle = UITableViewCellSelectionStyleGray;
            }
            cell.textLabel.font = [UIFont systemFontOfSize:14];
        }
  
        [(CAppListCell *)cell setShowDic:obj];
//        cell.imageView.image =[UIImage imageNamed:@"icon_120.png"];
////        [cell.imageView setImageWithURL:[NSURL URLWithString:[obj objectForKey:@"iconImage"]] placeholderImage:[UIImage imageNamed:@"icon_120.png"]];
//        cell.textLabel.text = [obj objectForKey:@"title"];
//        cell.detailTextLabel.text = [obj objectForKey:@"description"];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    id obj = [[menuCellObjArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if ([[obj class] isSubclassOfClass:[ERMeunCellObj class]]) {
        ERMeunCellObj *cellObj = (ERMeunCellObj *)obj;
        if (cellObj.action)
        {
            [self performSelector:cellObj.action];
        }
    }
    else if ([[obj class] isSubclassOfClass:[NSDictionary class]]){
        
        [MobClick event:@"RecommendApp" label:[obj objectForKey:@"title"]];
        NSString * urlStr = [obj objectForKey:@"downUrl"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    }

}

#pragma mark - -Action
- (void)imageManagerAction
{
    CAppDelegate *delegate = (CAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.centerViewController managerAction];
}

- (void)feedBackAction
{
    CAppDelegate *delegate = (CAppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.centerViewController feedBackAction];
}

//好评
- (void)evaluateAction
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ReviewURL]];
}


@end
