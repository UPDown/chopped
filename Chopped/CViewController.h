//
//  CViewController.h
//  Chopped
//
//  Created by helfy on 14-5-23.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CViewController : UIViewController

- (void)managerAction;
- (void)feedBackAction;
@end
