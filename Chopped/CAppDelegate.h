//
//  CAppDelegate.h
//  Chopped
//
//  Created by helfy on 14-5-23.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
#import "CViewController.h"
#import "CMenuViewController.h"

@interface CAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) MMDrawerController     * drawerController;
@property (nonatomic,strong) CViewController        * centerViewController;
@property (nonatomic,strong) CMenuViewController    * leftViewController;
@end
