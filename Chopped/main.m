//
//  main.m
//  Chopped
//
//  Created by helfy on 14-5-23.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAppDelegate class]));
    }
}
