//
//  CLoadAnimation.h
//  Chopped
//
//  Created by helfy on 14-6-4.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLoadAnimation : UIView
- (id)initWithFrameForAutoWidth:(CGRect)frame;
@end
