//
//  CChoopedView.m
//  Chopped
//
//  Created by helfy on 14-5-26.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CChoopedView.h"

@implementation CChoopedView

- (id)initWithType:(ChoopedType)type point:(CGPoint )point
{
    self = [super init];

    if (self) {
        self.choopedType =  type;
        self.frame = CGRectMake(point.x, point.y, 200, 200);
        [self creatCropView];
        [self creatFloatingBox];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame type:(ChoopedType)type
{
    self = [super initWithFrame:frame];
    if (self) {
        self.choopedType =  type;
        [self creatCropView];
        [self creatFloatingBox];
    }
    return self;
}

-(void)creatFloatingBox
{
    floatingBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150 , 30)];
    
    
    if(self.choopedType==ChoopedTypeTable)
    {
    
        floatingBox.frame = CGRectMake( floatingBox.frame.origin.x,  floatingBox.frame.origin.y, floatingBox.frame.size.width, 45);
    }
}
-(void)creatCropView
{
    cropView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
}

-(void)addGestureRecognizers
{
    //pan 控制移动
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
    [self addGestureRecognizer:pan];
}

-(void)panGestureRecognizer:(UIPanGestureRecognizer *)pan
{
    
}


- (BOOL)canBecomeFirstResponder;
{
    return YES;
}
- (BOOL)becomeFirstResponder{
    return YES;
}

- (BOOL)canResignFirstResponder{
    return YES;
}
- (BOOL)resignFirstResponder{
    return YES;
}
@end
