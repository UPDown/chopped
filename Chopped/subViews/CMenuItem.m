//
//  CMenuItem.m
//  Chopped
//
//  Created by helfy on 14-6-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CMenuItem.h"

@implementation CMenuItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:titleLabel];
        titleLabel.font = [UIFont systemFontOfSize:14];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        subTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        subTitleLabel.font = [UIFont systemFontOfSize:12];
        subTitleLabel.backgroundColor = [UIColor clearColor];
        subTitleLabel.textColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1];
        subTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:subTitleLabel];
    }
    return self;
}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    if(subTitleLabel.text == nil || subTitleLabel.text.length ==0)
    {
        titleLabel.frame = self.bounds;
    }
    else{
        titleLabel.frame = CGRectMake(0, 0, frame.size.width,  frame.size.height/2);
        subTitleLabel.frame = CGRectMake(0, frame.size.height/2, frame.size.width, frame.size.height/2);
    }
    
    
}
-(void)setTitle:(NSString *)title subTitle:(NSString *)subTitle
{
    titleLabel.text=title;
    subTitleLabel.text=subTitle;
 
    [self setFrame:self.frame];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
