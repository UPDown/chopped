//
//  CLoadAnimation.m
//  Chopped
//
//  Created by helfy on 14-6-4.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CLoadAnimation.h"

@implementation CLoadAnimation
{
    NSMutableArray *subViews;
    NSTimer *animationTimer;
    int animationStep;
     int itemWidth;
    
    BOOL isNone;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        subViews=[NSMutableArray array];
        itemWidth = 20;
        [self creatSubViews];
        isNone = NO;
        
    }
    return self;
}
- (id)initWithFrameForAutoWidth:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        subViews=[NSMutableArray array];
        itemWidth = frame.size.width/3;
        [self creatSubViewsForNone];
        isNone = YES;
        
    }
    return self;
}

-(UIColor *)colorForIndex:(int)index
{//出于效率和内存考虑  没使用array
    switch (index) {
        case 0:
            return RGB(238, 185, 17, 1);
            break;
        case 1:
               return RGB(222, 107, 27, 1);
            break;
        case 2:
               return RGB(223, 53 , 47, 1);
            break;
        case 3:
               return RGB(31, 178 ,136, 1);
            break;
        case 4:
               return RGB(228, 228, 228, 1);
            break;
        case 5:
               return RGB(45, 196, 93, 1);
            break;
        case 6:
               return RGB(44, 132, 210, 1);
            break;
        case 7:
               return RGB(135, 62, 166, 1);
            break;
        case 8:
               return RGB(39, 56, 75, 1);
            break;

        default:
            break;
    }
        return RGB(255, 255, 255, 1);
}

-(void)creatSubViews
{
    for (int i=0;i<9; i++) {
        UIView *subItem = [[UIView alloc] initWithFrame: CGRectMake(0, 0, itemWidth, itemWidth)];
        subItem.backgroundColor = [UIColor colorWithRed:i/10.0 green:i/10.0 blue:i/10.0 alpha:1];
        subItem.frame = CGRectMake((itemWidth+5)*(i%3), (itemWidth+5)*(i/3), itemWidth, itemWidth);
        [self addSubview: subItem];
        [subViews addObject:subItem];
    }
}
-(void)creatSubViewsForNone
{
    for (int i=0;i<9; i++) {
        UIView *subItem = [[UIView alloc] initWithFrame: CGRectMake(0, 0, itemWidth, itemWidth)];
        subItem.backgroundColor = [UIColor colorWithRed:i/10.0 green:i/10.0 blue:i/10.0 alpha:1];
        subItem.frame = CGRectMake((itemWidth)*(i%3), (itemWidth)*(i/3), itemWidth, itemWidth);
        [self addSubview: subItem];
        [subViews addObject:subItem];
    }
}
-(void)didMoveToSuperview
{
    [self startAnimation];
    [super didMoveToSuperview];
}
-(void)willMoveToSuperview:(UIView *)newSuperview{
       [self stopAnimatio];
    [super willMoveToSuperview:newSuperview];
}
-(void)startAnimation
{
    [self stopAnimatio];
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:isNone?@selector(onAnimationTimerForNone):@selector(onAnimationTimer) userInfo:nil repeats:YES];
    [animationTimer fire];
}
-(void)stopAnimatio
{
    if(animationTimer)
    {
        [animationTimer invalidate];
        animationTimer = nil;
    }
}
-(void)onAnimationTimer
{
    int path[] = {0, 1, 2, 5, 8, 7, 6, 3};
    
    int startIdx = 0;
    int prevIdx = 0;
    
    animationStep++;
    
    prevIdx = path[animationStep % (subViews.count - 1)];
    startIdx = path[(animationStep + 1) % (subViews.count - 1)];
    
    if (prevIdx == startIdx) {
        startIdx = prevIdx;
    }
    
    for (int i=0; i<subViews.count; i++) {
        UIView *particleView = subViews [i];
        
        
        
        if (i == prevIdx) {
            particleView.backgroundColor = [[self colorForIndex:i] colorWithAlphaComponent:0.9];
        } else if (i == startIdx) {
            particleView.backgroundColor = [self colorForIndex:i];
        } else {
            particleView.backgroundColor =  [[self colorForIndex:i] colorWithAlphaComponent:0.8];
        }
    }
}
-(void)onAnimationTimerForNone
{
    int path[] = {0, 1, 2, 3, 4, 5, 6, 7,8};
    
    int startIdx = 0;
    int prevIdx = 0;
    
    animationStep++;
    
    prevIdx = path[animationStep % (subViews.count - 1)];
    startIdx = path[(animationStep + 1) % (subViews.count - 1)];
    
    if (prevIdx == startIdx) {
        startIdx = prevIdx;
    }
    
    for (int i=0; i<subViews.count; i++) {
        UIView *particleView = subViews [i];
        
        
        
        if (i == prevIdx) {
            particleView.backgroundColor = [[self colorForIndex:i] colorWithAlphaComponent:0.9];
        } else if (i == startIdx) {
            particleView.backgroundColor = [self colorForIndex:i];
        } else {
            particleView.backgroundColor =  [[self colorForIndex:i] colorWithAlphaComponent:0.8];
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
