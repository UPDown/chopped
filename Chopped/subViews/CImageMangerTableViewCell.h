//
//  CImageMangerTableViewCell.h
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CChoppedObj;
@protocol CImageMangerTableViewCellDelegate <NSObject>

-(void)saveImageToAlbum:(CChoppedObj *)obj;
-(void)deleteImageToAlbum:(CChoppedObj *)obj;
@end

@interface CImageMangerTableViewCell : UITableViewCell
{
    UIImageView *thumbnailImage;
    UILabel *sizeLabel;
    UILabel *creatDate;
    UIButton *downButton;
    
    CChoppedObj *showObj;
}
-(void)setShowObj:(CChoppedObj *)obj;

@property (nonatomic,assign) id<CImageMangerTableViewCellDelegate> saveDelegate;
@end
