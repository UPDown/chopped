//
//  MIADViewer.m
//  MIViewer
//
//  Created by zym on 13-9-22.
//  Copyright (c) 2013年 MIViewer. All rights reserved.
//

#import "MIADViewer.h"
//#import "MobClick.h"
static MIADViewer *shareADView = nil;
@implementation MIADViewer
@synthesize delegate;
#define kAdViewPortraitRect CGRectMake(0, 0, 320, 50)
+(MIADViewer *)shareADView
{
    if(shareADView == nil)
    {
        shareADView= [[MIADViewer alloc] initWithFrame:CGRectZero];
    }
    return shareADView;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.hidden = YES;
        [self loadGoumobAd];
    }
    return self;
}
-(void)dealloc
{

    ObjRelease(guomobBannerAD);
}
#pragma guomob
-(void)loadGoumobAd
{
    [guomobBannerAD removeFromSuperview];
    ObjRelease(guomobBannerAD);
    
    self.frame =kAdViewPortraitRect;
    guomobBannerAD=[GuomobAdSDK initWithAppId:kGguomobAdKey delegate:self];
    [guomobBannerAD loadAd:YES];
    [self addSubview:guomobBannerAD];
}
- (void)loadBannerAdSuccess:(BOOL)success
{
    if(success)
    {
    self.hidden = NO;
        if(self.delegate && [self.delegate respondsToSelector:@selector(adLoadFilish:)])
        {
            [self.delegate adLoadFilish:self];
        }
    }
}
- (void)BannerConnectionDidFailWithError:(NSString *)error
{
     self.hidden = YES;
    self.frame = CGRectZero;
    if(self.delegate && [self.delegate respondsToSelector:@selector(loadAdError:)])
    {
        [self.delegate loadAdError:error];
    }
}
@end
