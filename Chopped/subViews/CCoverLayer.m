//
//  CCoverLayer.m
//  Chopped
//
//  Created by HDJ on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CCoverLayer.h"

@implementation CCoverLayer
@synthesize isDashed;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        cropLine = 3;
        cropRow = 3;
        isDashed = YES;
    }
    return self;
}

- (void)setIsDashed:(BOOL)_isDashed
{
    isDashed = _isDashed;
    [self setNeedsDisplay];
}

- (void)setLine:(int)line withRow:(int)row
{
    cropLine = line;
    cropRow = row;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context=UIGraphicsGetCurrentContext();
    //绘制剪裁区域外半透明效果

    //绘制剪裁区域的格子
    CGContextSetRGBStrokeColor(context, 240/255.0, 240/255.0, 240/255.0, 1.0f);
    CGContextSetLineWidth(context, LineWidth);
    
//    CGContextAddRect(context, self.bounds);
    
    if (self.isDashed) {
        //无缝
         const CGFloat lengths[] = {4,4};
        CGContextSetLineWidth(context, 1.0);
        CGContextSetLineDash(context, 0, lengths, 2);
        
        //列
        if (cropLine > 0) {
            
            float lastW = rect.size.width;
            float itemW = lastW/cropLine;
            
            for (int i = 1; i < cropLine; i++) {
                CGContextMoveToPoint(context, self.bounds.origin.x + itemW * i, self.bounds.origin.y);
                CGContextAddLineToPoint(context, self.bounds.origin.x + itemW * i, self.bounds.origin.y+self.bounds.size.height);
            }
        }
        
        //行
        if (cropRow > 0) {
            
            float lastH = rect.size.height;
            float itemH = lastH/cropRow;
            
            for (int i = 1; i < cropRow; i++) {
                CGContextMoveToPoint(context, self.bounds.origin.x, self.bounds.origin.y + itemH * i);
                CGContextAddLineToPoint(context, self.bounds.origin.x+self.bounds.size.width, self.bounds.origin.y + itemH * i);
            }
        }
        
    }
    else{
        //有缝
        
        //列
        if (cropLine > 0) {
            
            float lastW = rect.size.width - (cropLine - 1) * LineWidth;
            float itemW = lastW/cropLine;
            
            for (int i = 1; i < cropLine; i++) {
                CGContextMoveToPoint(context, self.bounds.origin.x + (itemW * i + LineWidth * (i - 1)) + LineWidth/2, self.bounds.origin.y);
                CGContextAddLineToPoint(context, self.bounds.origin.x + (itemW * i + LineWidth * (i - 1)) + LineWidth/2, self.bounds.origin.y+self.bounds.size.height);
            }
        }
        
        //行
        if (cropRow > 0) {
            
            float lastH = rect.size.height - (cropRow - 1) * LineWidth;
            float itemH = lastH/cropRow;
            
            for (int i = 1; i < cropRow; i++) {
                CGContextMoveToPoint(context, self.bounds.origin.x, self.bounds.origin.y + (itemH * i + LineWidth * (i - 1)) + LineWidth/2);
                CGContextAddLineToPoint(context, self.bounds.origin.x+self.bounds.size.width, self.bounds.origin.y + (itemH * i + LineWidth * (i - 1)) + LineWidth/2);
            }
        }
    }

    CGContextStrokePath(context);
    
}


@end
