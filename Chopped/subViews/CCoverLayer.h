//
//  CCoverLayer.h
//  Chopped
//
//  Created by HDJ on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LineWidth   4.0

@interface CCoverLayer : UIView
{
    int cropLine;
    int cropRow;
}

@property(nonatomic,assign)BOOL isDashed;       //虚线

- (void)setLine:(int)line withRow:(int)row;
@end
