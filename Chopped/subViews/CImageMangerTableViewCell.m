//
//  CImageMangerTableViewCell.m
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CImageMangerTableViewCell.h"
#import "CChoppedObj.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
@implementation CImageMangerTableViewCell
{
    CAGradientLayer *gradient;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
//        UIImageView *thumbnailImage;
//        UILabel *sizeLabel;
//        UILabel *cellSizeLabel;
//        UILabel *creatDate;
//        UIButton *downButton;
        gradient = [CAGradientLayer layer];
        gradient.frame = self.bounds;
  
        gradient.colors = [NSArray arrayWithObjects:
                           (id)[UIColor colorWithRed:240/255.0
                                               green:240/255.0
                                                blue:240/255.0
                                               alpha:1.0].CGColor,
                           (id)[UIColor colorWithRed:220/255.0
                                               green:220/255.0
                                                blue:220/255.0
                                               alpha:1].CGColor,
                         
                           nil];

        [self.layer insertSublayer:gradient atIndex:0];

        
        
        self.backgroundColor = [UIColor clearColor];
        thumbnailImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 35, 200, 200)];
        sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 150, 30)];
        creatDate = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 150, 30)];
        creatDate.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        creatDate.font = [UIFont systemFontOfSize:12];
        downButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
//        downButton.backgroundColor = [UIColor colorWithRed:29.0/255.0
//                                                     green:173.0/255.0
//                                                      blue:234.0/255.0
//                                                     alpha:1.0];
//        [downButton setTitle:@"保存切图至相册" forState:UIControlStateNormal];
        downButton.titleLabel.font =[UIFont systemFontOfSize:14];
//        [downButton addTarget:self action:@selector(downImages) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:thumbnailImage];
        [self addSubview:sizeLabel];
        [self addSubview:creatDate];
        [self addSubview:downButton];
        
    }
    return self;
}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    gradient.frame = self.bounds;
    
}
-(NSString *)strForTimeInterval:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yy/MM/dd HH:mm"];
    NSString *str =[formatter stringFromDate:date];
    return str;
}
-(void)setShowObj:(CChoppedObj *)obj
{
    showObj = obj;
    NSString *thumbPath =[[obj getObjSavePath] stringByAppendingPathComponent:@"thumb.png"];
    thumbnailImage.image = [UIImage imageWithContentsOfFile:thumbPath];
//    int maxWidth =kDecIsiPhone5?280:240;
//    int imageMaxWidth =thumbnailImage.image.size.width > thumbnailImage.image.size.height?thumbnailImage.image.size.width :thumbnailImage.image.size.height;
//    CGSize  imageSize = CGSizeMake(thumbnailImage.image.size.width*maxWidth/imageMaxWidth, thumbnailImage.image.size.height*maxWidth/imageMaxWidth);
     CGSize  imageSize = CGSizeMake(obj.thumbImageWidht,obj.thumbImageHeigth);
    thumbnailImage.frame = CGRectMake((self.frame.size.width-imageSize.width)/2, thumbnailImage.frame.origin.y, imageSize.width, imageSize.height);
    
    creatDate.text = [self strForTimeInterval:obj.creatDate];
    
    downButton.frame = CGRectMake((self.frame.size.width-downButton.frame.size.width)/2, CGRectGetMaxY(thumbnailImage.frame)+5, downButton.frame.size.width, downButton.frame.size.height);
}
-(void)downImages
{
    if([self.saveDelegate respondsToSelector:@selector(saveImageToAlbum:)])
    {
        [self.saveDelegate saveImageToAlbum:showObj];
    }
}

- (void)deleteImages
{
    if([self.saveDelegate respondsToSelector:@selector(deleteImageToAlbum:)])
    {
        [self.saveDelegate deleteImageToAlbum:showObj];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    
    [UIView beginAnimations:nil context:nil];
    if (editing) {
        [downButton setTitle:@"删除" forState:UIControlStateNormal];
        downButton.backgroundColor = [UIColor colorWithRed:255.0/255.0
                                                     green:0/255.0
                                                      blue:0/255.0
                                                     alpha:1.0];
        [downButton removeTarget:self action:@selector(downImages) forControlEvents:UIControlEventTouchUpInside];
        [downButton addTarget:self action:@selector(deleteImages) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        downButton.backgroundColor = [UIColor colorWithRed:29.0/255.0
                                                     green:173.0/255.0
                                                      blue:234.0/255.0
                                                     alpha:1.0];
        [downButton setTitle:@"保存切图至相册" forState:UIControlStateNormal];
        [downButton removeTarget:self action:@selector(deleteImages) forControlEvents:UIControlEventTouchUpInside];
        [downButton addTarget:self action:@selector(downImages) forControlEvents:UIControlEventTouchUpInside];
    }
    [UIView commitAnimations];
}

@end
