//
//  CMenuItem.h
//  Chopped
//
//  Created by helfy on 14-6-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMenuItem : UIButton
{
    UILabel *titleLabel;
    UILabel *subTitleLabel;
}
-(void)setTitle:(NSString *)title subTitle:(NSString *)subTitle;
@end
