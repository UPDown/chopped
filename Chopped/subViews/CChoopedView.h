//
//  CChoopedView.h
//  Chopped
//
//  Created by helfy on 14-5-26.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    ChoopedTypeRect=0,
    ChoopedTypeRound,
    ChoopedTypeTable
}ChoopedType;

@interface CChoopedView : UIView
{
    //浮框
    UIView *floatingBox;
    UILabel *xLabel;
    UILabel *yLabel;
    UILabel *wLabel;
    UILabel *hLabel;
    UILabel *tableCountLabe;  //show when tableType
    
    //剪切的view
    UIView *cropView;
    
    CGPoint startPoint;
}
@property(nonatomic,assign)ChoopedType choopedType;
- (id)initWithFrame:(CGRect)frame type:(ChoopedType)type;
- (id)initWithType:(ChoopedType)type point:(CGPoint )point;
@end
