//
//  CMeunView.m
//  Chopped
//
//  Created by helfy on 14-6-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CMeunView.h"
#import "CMenuItem.h"
@implementation CMeunView
{
    NSMutableArray *muenItems;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        self.backgroundColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.3];
        muenItems =[[NSMutableArray alloc] init];
        
        [self creatItems];
    }
    return self;
}

-(void)creatItems
{
   BOOL isDashed = [[NSUserDefaults standardUserDefaults] boolForKey:kIsDashedKey];
    
    NSArray *titleArray = @[@"格子数",(isDashed?@"无缝":@"有缝"),@"完成"];

    NSArray *subTitleArray = @[@"", @"",@""];
    
    UIScrollView *contentView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];

    [self addSubview:contentView];
    int totleWidth =24+(50+12)*titleArray.count;
    
    contentView.contentSize = CGSizeMake(totleWidth, 0);
    
    if(totleWidth < contentView.frame.size.width)
    {
        contentView.frame =CGRectMake((contentView.frame.size.width-totleWidth)/2, 0, totleWidth, contentView.frame.size.height);
    }
    
    for (int i=0 ; i< titleArray.count; i++) {
        CMenuItem *item = [CMenuItem buttonWithType:UIButtonTypeCustom];
        item.frame = CGRectMake(12+(50+12)*(i), 12, 50, 50);
        item.backgroundColor =  [UIColor colorWithRed:29.0/255.0
                                                green:173.0/255.0
                                                 blue:234.0/255.0
                                                alpha:1.0];
        [item setShowsTouchWhenHighlighted:YES];
        [item setTitle:titleArray[i] subTitle:subTitleArray[i] ];
        [item addTarget:self action:@selector(mentItemSelect:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:item];
        item.tag = i;
        [muenItems addObject:item];
    }
}


-(void)mentItemSelect:(CMenuItem *)item
{
    if(self.menuDelegate && [self.menuDelegate respondsToSelector:@selector(menuItemDidTap:)])
    {
        [self.menuDelegate menuItemDidTap:item];
    }
}
@end
