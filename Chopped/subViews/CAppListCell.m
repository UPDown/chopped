//
//  CAppListCell.m
//  Chopped
//
//  Created by helfy on 14-6-6.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CAppListCell.h"
#import "UIImageView+WebCache.h"
@implementation CAppListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
        [self addSubview:iconImageView];
        iconImageView.layer.cornerRadius = 8;
        iconImageView.layer.masksToBounds = YES;
        titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(60, 5, 220, 25)];
        [self addSubview:titleLabel];
        titleLabel.font = [UIFont systemFontOfSize:14];
        desLabel=[[UILabel alloc] initWithFrame:CGRectMake(60, 25, 220, 20)];
        
        [self addSubview:desLabel];
        desLabel.font = [UIFont systemFontOfSize:10];
        desLabel.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1];
        
        titleLabel.backgroundColor = [UIColor clearColor];
        desLabel.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(void)setShowDic:(NSDictionary *)dic
{
    [iconImageView setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"iconImage"]] placeholderImage:[UIImage imageNamed:@"icon_120.png"]];
    titleLabel.text = [dic objectForKey:@"title"];
    desLabel.text = [dic objectForKey:@"description"];
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
