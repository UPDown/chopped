//
//  CAppListCell.h
//  Chopped
//
//  Created by helfy on 14-6-6.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAppListCell : UITableViewCell
{
    UIImageView *iconImageView;
    UILabel *titleLabel;
    UILabel *desLabel;
}
-(void)setShowDic:(NSDictionary *)dic;
@end
