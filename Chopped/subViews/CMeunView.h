//
//  CMeunView.h
//  Chopped
//
//  Created by helfy on 14-6-3.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CMenuItem;
@protocol CMeunViewDelegate <NSObject>

-(void)menuItemDidTap:(CMenuItem *)item;

@end

@interface CMeunView : UIView
@property(nonatomic,assign)id<CMeunViewDelegate> menuDelegate;
@end
