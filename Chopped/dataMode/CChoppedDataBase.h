//
//  CChoppedDataBase.h
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseQueue.h"
@class CChoppedObj;
//管理 切好的obj
@interface CChoppedDataBase : NSObject
{
    FMDatabaseQueue *dbQueue;
}
+ (CChoppedDataBase *)shareDBControl;
+ (void)DBControlRelease;

- (void)creatTable;//建表
- (NSArray *)queryAllChoppedObjs;
- (void)deleteChoppedObj:(CChoppedObj *)obj;
- (void)insertChoppedObj:(CChoppedObj *)obj;

@end
