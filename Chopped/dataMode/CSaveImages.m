//
//  CSaveImages.m
//  Chopped
//
//  Created by helfy on 14-6-5.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CSaveImages.h"
#import "CChoppedObj.h"
#import "MBProgressHUD.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
@implementation CSaveImages

// toAtlbum   管理里面保存至相册用
+(void)saveImage:(CChoppedObj *)obj forIndex:(int)index completion:(void (^)(void))completion
{
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    NSString *subPath = [obj getObjSavePath];
    
    NSString *path = [NSString stringWithFormat:@"%@/%i.png",subPath,index];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    int dd =index +1;
    [assetsLibrary saveImage:image toAlbum:@"切切" completion:^(NSURL *assetURL, NSError *error) {
        if(dd < obj.rowCount * obj.columnCount)
        {
            [self saveImage:obj forIndex:dd completion:completion];
        }
        else
        {
            completion();
        }
    } failure:^(NSError *error) {
        
    }];
}
//生成图片的时候 保存至相册
+(void)saveImagesForImageArray:(NSArray *)imageArray forIndex:(int)index completion:(void (^)(void))completion
{
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
 
    UIImage *image = [imageArray objectAtIndex:index];
    int dd =index +1;
    [assetsLibrary saveImage:image toAlbum:@"切切" completion:^(NSURL *assetURL, NSError *error) {
        if(dd < imageArray.count)
        {
            [self saveImagesForImageArray:imageArray forIndex:dd completion:completion];
        }
        else
        {
            completion();
        }
    } failure:^(NSError *error) {
        
    }];
}
//保存至documents
+(void)saveImages:(NSArray *)imageArray toPath:(NSString *)path
{
    for (int i  = 0; i < imageArray.count;i++) {
    UIImage *image = [imageArray objectAtIndex:i];
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile:[NSString stringWithFormat:@"%@/%i.png",path,i] atomically:YES];
    }
}

@end
