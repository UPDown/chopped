//
//  CChoppedObj.m
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CChoppedObj.h"
#import "CChoppedDataBase.h"
@implementation CChoppedObj

-(id)init
{
    self= [super init];
    if(self)
    {
        self.objId = [self get_uuid];
        [self defuleValue];
    }
    return self;
}
-(id)initForNilId
{
    self= [super init];
    if(self)
    {
        [self defuleValue];
    }
    return self;
}
-(void)reSetId
{
   self.objId = [self get_uuid];
}
-(void)defuleValue
{
    self.rowCount = 3;
    self.columnCount = 3;
    
    self.cellSzieForWidth = 100;
    self.cellSzieForHight = 100;
}
-(NSString *)get_uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(nil);
    CFStringRef stringRef = CFUUIDCreateString(nil, uuidRef);
    NSString *uuid = [NSString stringWithString:(__bridge NSString*)stringRef];
    CFRelease(uuidRef);
    CFRelease(stringRef);
    
    return uuid;
}
-(NSTimeInterval)get_Time
{
    NSDate *date = [NSDate date];
    return [date timeIntervalSince1970];
}

-(NSString *)getThumbImagePath
{
    return [[self getObjSavePath] stringByAppendingPathComponent:@"thumb.png"];
}
-(NSString *)getObjSavePath
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    path=[path stringByAppendingPathComponent:self.objId];
    return path;
}
-(void)setNewImage:(UIImage *)image
{
    self.objId = [self get_uuid];
    self.choppedImage=image;
    if(self.delegate && [self.delegate respondsToSelector:@selector(choppedImageChange)])
    {
        [self.delegate choppedImageChange];
    }
}
-(void)setNewRow:(int)newRow column:(int)newColume
{
    self.rowCount = newRow;
    self.columnCount = newColume;
    if(self.delegate && [self.delegate respondsToSelector:@selector(rowAndColumnDidChange)])
    {
        [self.delegate rowAndColumnDidChange];
    }
}
-(void)setCellSzieWidth:(int)width Hight:(int)hight
{
    self.cellSzieForHight = hight;
    self.cellSzieForWidth = width;
    if(self.delegate && [self.delegate respondsToSelector:@selector(sizeDidChange)])
    {
        [self.delegate sizeDidChange];
    }
}
-(void)deleteSelf
{
    [[CChoppedDataBase shareDBControl] deleteChoppedObj:self];
    [[NSFileManager defaultManager] removeItemAtPath:[self getObjSavePath] error:nil];
}
-(void)choppedImageFinish
{
    self.creatDate = [self get_Time];
  
    [[CChoppedDataBase shareDBControl] insertChoppedObj:self];
}
@end
