//
//  ERMeunCellObj.h
//  ERReadder
//
//  Created by zym on 13-12-22.
//  Copyright (c) 2013年 GR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ERMeunCellObj : NSObject
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)UIImage *image;
@property (nonatomic,assign)int level;
@property (nonatomic,assign)SEL action;
@property (nonatomic,assign)BOOL isCatalog;

+(ERMeunCellObj *)cellObjWith:(NSString *)title level:(int)level action:(SEL)action;
@end
