//
//  CChoppedDataBase.m
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CChoppedDataBase.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"
#import "CChoppedObj.h"
#define KCreatBookTableSQL  @"CREATE TABLE IF NOT EXISTS choppedTable(objIndex INTEGER PRIMARY KEY AUTOINCREMENT,objId TEXT,rowCount INTEGER,columnCount INTEGER,downCount INTEGER,creatDate INTEGER,cellSzieForWidth INTEGER,cellSzieForHight INTEGER,thumbImageWidht INTEGER,thumbImageHeigth INTEGER)"


static CChoppedDataBase *dbControll = nil;
@implementation CChoppedDataBase

+ (CChoppedDataBase *)shareDBControl
{
    @synchronized(self)
    {
        if (dbControll == nil) {
            dbControll = [[CChoppedDataBase alloc]init];
        }
    }
    return dbControll;
}

+ (void)DBControlRelease
{
    dbControll = nil;
}

- (void)dealloc
{
    dbQueue=nil;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSString *dbpath = [[NSString alloc] initWithString:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/chopped.db"]];
        dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbpath];
        [self creatTable];

    }
    return self;
}

-(FMDatabaseQueue *)dbQueue{
    return dbQueue;
}
- (void)creatTable
{
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = KCreatBookTableSQL;
        [db executeUpdate:sql];
        [db close];
    }];
}


- (NSArray *)queryAllChoppedObjs
{
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        NSString *sql = @"select * from choppedTable where 1 order by objIndex desc";
        
        
        FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            CChoppedObj *obj = [[CChoppedObj alloc] initForNilId];
            obj.objId =[rs stringForColumn:@"objId"] ;
            obj.rowCount = [rs intForColumn:@"rowCount"];
            obj.columnCount = [rs intForColumn:@"columnCount"];
            obj.downCount = [rs intForColumn:@"downCount"];
            obj.creatDate = [rs intForColumn:@"creatDate"];
            obj.rowCount = [rs intForColumn:@"rowCount"];
            obj.cellSzieForHight = [rs intForColumn:@"cellSzieForHight"];
            obj.cellSzieForWidth = [rs intForColumn:@"cellSzieForWidth"];
            obj.rowCount = [rs intForColumn:@"rowCount"];
            obj.thumbImageWidht = [rs intForColumn:@"thumbImageWidht"];
            obj.thumbImageHeigth = [rs intForColumn:@"thumbImageHeigth"];
            [dataArray addObject:obj];
        }
        [db close];
    }];
    return dataArray;
}

- (void)deleteChoppedObj:(CChoppedObj *)obj
{
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        [db open];
        NSString *sql = [NSString stringWithFormat:@"delete from choppedTable where objId='%@'",obj.objId];
        [db executeUpdate:sql];
        [db close];
    }];
}
#define kInsrtObjSQL  @"INSERT INTO choppedTable(objId, rowCount, columnCount,downCount, creatDate,cellSzieForWidth,cellSzieForHight,thumbImageWidht,thumbImageHeigth) VALUES (?,?,?,?,?,?,?,?,?)"
- (void)insertChoppedObj:(CChoppedObj *)obj
{
    [self.dbQueue inDatabase:^(FMDatabase *db){
        [db open];
        
        [db executeUpdate:kInsrtObjSQL,obj.objId,[NSNumber numberWithInt:obj.rowCount],[NSNumber numberWithInt:obj.columnCount],[NSNumber numberWithInt:obj.downCount],[NSNumber numberWithInt:obj.creatDate],[NSNumber numberWithInt:obj.cellSzieForWidth],[NSNumber numberWithInt:obj.cellSzieForHight],[NSNumber numberWithInt:obj.thumbImageWidht],[NSNumber numberWithInt:obj.thumbImageHeigth]];
        [db close];
    }];
}
@end
