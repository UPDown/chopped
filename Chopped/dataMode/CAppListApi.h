//
//  CAppListApi.h
//  Chopped
//
//  Created by helfy on 14-6-6.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CAppListApiDelegate <NSObject>

//json
-(void)ERApiDidFinish:(id)api  respone:(id)jsonStr;
//获取信息失败
-(void)ERApiDidFail:(id)api  error:(NSString *)errorStr;
@end

@interface CAppListApi : NSObject
{
    NSMutableData       *receiveData;
}
@property(nonatomic,assign)id<CAppListApiDelegate>  apiDelegate;

+ (CAppListApi *)defaultApi;

- (void)requestAppList:(id)sender;
@end
