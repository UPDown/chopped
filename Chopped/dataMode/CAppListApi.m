//
//  CAppListApi.m
//  Chopped
//
//  Created by helfy on 14-6-6.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import "CAppListApi.h"

@implementation CAppListApi
@synthesize apiDelegate;

+ (CAppListApi *)defaultApi
{
    static CAppListApi *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
         sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self=[super init]) {
        receiveData =[[NSMutableData alloc] init];
    }
    return self;
}

- (void)requestAppList:(id)sender
{
    self.apiDelegate = sender;
    
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    
    NSString *urlString =  [NSString stringWithFormat:@"http://helfyapplist.duapp.com/recommend.php?appName=%@",[appName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *requrst = [NSURLRequest requestWithURL:url];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:requrst delegate:self];
    if (!connection) {
        if (self.apiDelegate) {
            if([self.apiDelegate respondsToSelector:@selector(ERApiDidFail:error:)])
            {
                [self.apiDelegate ERApiDidFail:self error:@"获取失败"];
            }
        }
        
        self.apiDelegate = nil;
    }
}

#pragma mark-
#pragma NSUrlConnectionDelegate methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //接受返回数据，这个方法可能会被调用多次，因此将多次返回数据加起来
    
//    NSUInteger datalength = [data length];

    [receiveData appendData:data];;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //连接结束
    
    NSLog(@"连接结束");
    if (receiveData) {
        id objDic = [NSJSONSerialization JSONObjectWithData:receiveData options:NSJSONReadingMutableLeaves error:nil];
        
        NSLog(@"返回数据量：%@", objDic);
        if (self.apiDelegate) {
            if([self.apiDelegate respondsToSelector:@selector(ERApiDidFinish:respone:)])
            {
                [self.apiDelegate ERApiDidFinish:self respone:objDic];
            }
        }
    }
    else{
        if (self.apiDelegate) {
            if([self.apiDelegate respondsToSelector:@selector(ERApiDidFail:error:)])
            {
                [self.apiDelegate ERApiDidFail:self error:@"获取失败"];
            }
        }
    }
    self.apiDelegate = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //链接错误
    NSLog(@"链接错误");
    if (self.apiDelegate) {
        if([self.apiDelegate respondsToSelector:@selector(ERApiDidFail:error:)])
        {
            [self.apiDelegate ERApiDidFail:self error:@"获取失败"];
        }
    }

    self.apiDelegate = nil;
}

@end
