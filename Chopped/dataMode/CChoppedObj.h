//
//  CChoppedObj.h
//  Chopped
//
//  Created by helfy on 14-5-29.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CChoppedObjDelegate <NSObject>

-(void)rowAndColumnDidChange;
-(void)sizeDidChange;
-(void)choppedImageChange;
@end

@interface CChoppedObj : NSObject
@property (nonatomic,copy) NSString *objId;  //ID 随机生成，对应其文件夹
@property (nonatomic,assign) int rowCount;  //行数
@property (nonatomic,assign) int columnCount;  //列数
@property (nonatomic,assign) int downCount;  //使用次数
@property (nonatomic,assign) int cellSzieForWidth;  //格子大小 宽
@property (nonatomic,assign) int cellSzieForHight;  //格子大小  高  前期两个值一样。
@property (nonatomic,assign) NSTimeInterval creatDate;  //创建次数


@property (nonatomic,copy) UIImage* choppedImage;  //图片
@property (nonatomic,assign) int thumbImageWidht;  //缩略图宽
@property (nonatomic,assign) int thumbImageHeigth;  //缩略图高
@property (nonatomic,assign)id<CChoppedObjDelegate> delegate;
-(id)initForNilId;
-(void)reSetId;
-(void)setNewRow:(int)newRow column:(int)newColume;
-(void)setCellSzieWidth:(int)width Hight:(int)hight;
-(void)deleteSelf;
-(NSString *)getThumbImagePath;


-(NSString *)getObjSavePath;  //obj对应文件夹  预览图： thumb.png   切图：1.png 2.png 。。。。。
-(void)choppedImageFinish;  //切图完成

-(void)setNewImage:(UIImage *)image;  //
@end
