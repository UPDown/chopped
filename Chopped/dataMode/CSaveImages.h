//
//  CSaveImages.h
//  Chopped
//
//  Created by helfy on 14-6-5.
//  Copyright (c) 2014年 helfy. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CChoppedObj;
@interface CSaveImages : NSObject

// toAtlbum   管理里面保存至相册用
+(void)saveImage:(CChoppedObj *)obj forIndex:(int)index completion:(void (^)(void))completion;
//生成图片的时候 保存至相册
+(void)saveImagesForImageArray:(NSArray *)imageArray forIndex:(int)index completion:(void (^)(void))completion;
//保存至documents
+(void)saveImages:(NSArray *)imageArray toPath:(NSString *)path;
@end
